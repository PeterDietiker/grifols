This is the repository for code used to evaluate data from the Grifols RDP project. 

# HowTo
See notebooks:

- example_evaluation_mobidiag.iypnb

# Installation
- Download and install miniconda from <https://docs.conda.io/en/latest/miniconda.html>
- open conda promt and creat environment with
```
conda env create -f environment.yml
```
- install jupyter extensions
```
jupyter contrib nbextension install --user
```
- start jupyter with
```
start_jupyter.bat
```
