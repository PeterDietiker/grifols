import os
import pandas as pd
from collections import OrderedDict
from glob import glob
from zipfile import ZipFile
import re
import numpy as np
import datetime
import statsmodels.formula.api as sm
import scipy.stats
import matplotlib.pyplot as plt
import mplcursors
import seaborn as sb
from bs4 import BeautifulSoup
from pathlib import Path
from datetime import datetime
import shutil
import logging

import natsort

class RDP():
    '''
    Class to analyze and plot data from the RDP. 
    '''
    def __init__(self,path=None, measurement = None,plate_map = None, bintime = 1e-4,channel="Red", logger = None):
        '''
        Parameters
        ---------
        path: path to the measurement zip 
        type path: str
        measurement: file name of the measurement zip
        type measurement: str
        plate_map: Name of the plate map
        type plate_map: str
        bintime: bin time used for the measurements (Default: 1e-4)
        type bintime: float
        channel: Channel used in this measurement
        type channel: str, either Blue, Red, Green or NIR
        '''
        self.measurement = measurement
        self.bintime = bintime
        self.channel = channel
        self.nr_bins = None
        self.ols = None
        self.LOD = None
        self.ax = None
        if path is not None:
            self.path = Path(path)
        else:
            self.path = Path(os.getcwd())
        self.plate_map = plate_map
        self.results = pd.DataFrame(columns=["labels","conc","events","dye","threshold"])
        self.suffix_dict = {"Blue": "B","Green":"G","Red":"R","NIR":"N"}
        self.params = None
        
        self.init_logger(logger)
        if self.measurement is not None:
            self._get_name()
            self._unzip_folder()
            self._load_data()
            self._load_focus_data()
            self._load_rdp_result()
            self._load_params()
            self._generate_results_tables()
            self._set_conc()
        self._isCalculated = False
        self.calc_params = dict(corrected=False,threshold=1,nsigma=5.5, windows = 1, stats = "Pois", normalisation = 60, vials_to_count = ["..+"])
        self.init_SMC_library()
    
    def init_SMC_library(self):
        main_folder = Path(__file__).parent
        dll_path = main_folder/"SMC_algorithm"/"SMCWrapper"/"SMCWrapper"/"SMCWrapper"/"bin"/"x64"/"Debug"/"SMCWrapper"
        #SMC_default_values_path = os.path.join(main_folder,"SMC_algorithm","DefaultValues.xml")
        #SMC_default_values_path =  main_folder/"SMC_algorithm"/"DefaultValues.xml"
        #self.SMC_actual_values_path = os.path.join(os.path.dirname(SMC_default_values_path),"ActualValues.xml")
        #self.SMC_actual_values_path = main_folder/"SMC_algorithm"/"ActualValues.xml"
        SMC_default_values_path = Path("C:\Daten\Python\RDP_simulator\SMC_algorithm\DefaultValues.xml")
        #self.SMC_actual_values_path = os.path.join(os.path.dirname(SMC_default_values_path),"ActualValues.xml")
        self.SMC_actual_values_path = Path("C:\Daten\Python\RDP_simulator\SMC_algorithm\ActualValues.xml")
        try:
            shutil.copy2(SMC_default_values_path, self.SMC_actual_values_path)
        except Exception:
            self.logger.error("Not able to copy SMC default values",  exc_info=True)
            return
        try:
            import clr
            clr.AddReference(str(dll_path))
            from Calculator import Calculator
            self.SMC_calculator = Calculator()
        except Exception:
            self.logger.error("Not able to load SMC calculator",  exc_info=True)
            self.SMC_calculator = None
        else:
            self.adjust_SMC_values()
    
    def init_logger(self, logger):

        if logger is None:
            self.logger = logging.getLogger(__name__)
            self.logger.handlers = []  # clear all handlers
            self.logger.setLevel(logging.DEBUG)
            logging_handler = logging.StreamHandler()
            logging_handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logging_handler.setFormatter(formatter)
            self.logger.addHandler(logging_handler)
        else:
            self.logger = logger
    
    
    @staticmethod
    def mad(x):
        '''
        calculates the median absulute deviation
        '''
        return np.median(np.fabs(x - np.median(x)))

    def _get_name(self):
        if ".zip" in self.measurement:
            self.measurement = self.measurement.split(".zip",1)[0]

    def _unzip_folder(self):
        try:
            mtime_folder = os.path.getmtime(os.path.join(self.path,self.measurement))
        except FileNotFoundError:
            mtime_folder = 0
        try:
            mtime_archive = os.path.getmtime(os.path.join(self.path,self.measurement+".zip"))
        except FileNotFoundError:
            print("zip Archive not found")
            return 1
        if mtime_archive > mtime_folder:
            with ZipFile(os.path.join(self.path,self.measurement+".zip"), 'r') as zipObj:
               zipObj.extractall(os.path.join(self.path,self.measurement))
    
    def _load_data(self):
        #check if this is a multi_channel dataset (files end with "B","G","N" or "R")
        
        
        
        def update(d, u):
            '''
            function for update of a nested dict
            from:
            https://stackoverflow.com/a/3233356
            '''
            import collections.abc
            for k, v in u.items():
                if isinstance(v, collections.abc.Mapping):
                    d[k] = update(d.get(k, {}), v)
                else:
                    d[k] = v
            return d
        
        file_list = glob(os.path.join(self.path,self.measurement,"APD_Read_Log_*_V1_*.txt"))
        multichannel = False
        if len(file_list) > 0:
            multichannel = True
            
        if multichannel:
            d = {}
            self.labels = list()
            idx = 0
            for key, value in self.suffix_dict.items():
                file_list = glob(os.path.join(self.path,self.measurement,f"APD_Read_Log_*_V1_{value}.txt"))
                if len(file_list) > 0:
                    labels = [re.findall("APD_Read_Log_([A-Z]\d+)_V1.*\.txt",fn)[0] for fn in file_list]
                    d_ = dict(zip(labels, [{key:pd.read_csv(fn, names=["Count_raw","Bin"],usecols=[0,1],index_col = 1)} for fn in file_list]))
                    d = update(d,d_)
                    self.labels = list(set().union(self.labels,labels))
                    idx += 1
        else:
            file_list = glob(os.path.join(self.path,self.measurement,f"APD_Read_Log_*_V1.txt"))
            
            if len(file_list) == 0:
                print("no files found")
                self.labels=[]
                self.data  = OrderedDict()
                self.nr_bins = 0
                #self.results = pd.DataFrame(columns=["labels"])
                return
            else:
                self.labels = [re.findall("APD_Read_Log_([A-Z]\d+)_V1.txt",fn)[0] for fn in file_list]
                test= pd.read_csv(file_list[0])
                simultaneous = True if len(test.columns) > 2 else False
                if simultaneous:
                    #this is a simultaneous read. It is assumed that all channels are read. 
                    #channel_dict = {"Blue":0,"Green":1,"Red":2,"NIR":3}
                    channel_dict = {"Green":0,"Red":1,"NIR":2} #blue channel not measured
                    d = {}
                    for label in self.labels:
                        fn = os.path.join(self.path,self.measurement,f"APD_Read_Log_{label}_V1.txt")
                        d[label] = {key:pd.read_csv(fn, names=["Count_raw","Bin"],usecols=[value,3],index_col = 1) for key, value in channel_dict.items()}
                else:
                    #it is assumed that only one channel (the one stored in self.channel) is read        
                    d = dict(zip(self.labels, [{self.channel:pd.read_csv(fn, names=["Count_raw","Bin"],usecols=[0,1],index_col = 1)} for fn in file_list]))
        
        self.data = OrderedDict(sorted(d.items(), key=lambda t: (t[0][0],t[0][1:])))
        min_bin = np.inf
        for key in self.data.keys():
            for _key in self.data[key].keys():
                if self.data[key][_key].shape[0]< min_bin:
                    min_bin = self.data[key][_key].shape[0]
        self.nr_bins = min_bin
        #self.nr_bins = np.array([self.data[key].shape[0] for key in self.data.keys()],dtype=int).min()
        self.labels = natsort.natsorted(self.labels)
        self.results = pd.DataFrame({"labels" : self.labels}, index = self.labels) 
        regex_column = re.compile("([A-Za-z]+).*")
        regex_row = re.compile("[A-Za-z]+([0-9]+)")
        self.results["column"] = self.results.labels.map(lambda x: regex_column.match(x).groups()[0])
        self.results["row"] = self.results.labels.map(lambda x: regex_row.match(x).groups()[0])
    
    def _load_focus_data(self):
        file_list = glob(os.path.join(self.path,self.measurement,f"APD_Focus_Log_*_V1.txt"))
        if len(file_list) == 0:
            print("no focus files found")
            return
        focus_labels = [re.findall("APD_Focus_Log_([A-Z]\d+)_V1.txt",fn)[0] for fn in file_list]
        for label in focus_labels:
            if label not in self.labels:
                self.labels.append(label)
                self.results = self.results.append(pd.DataFrame({"labels" : [label]}, index = [label]) )
        d = dict(zip(self.labels, [pd.read_csv(fn, names=["Count_raw","Position"],usecols=[0,1],index_col = 1) for fn in file_list]))
        self.focus_data = OrderedDict(sorted(d.items(), key=lambda t: (t[0][0],t[0][1:])))
        
        
        #load RDP results if available
    def _load_rdp_result(self):
        '''
        loads a result file from the rdp software
        '''
        result_fn = os.path.join(self.path,self.measurement+"_Result.csv")
        try:
            test = pd.read_csv(result_fn)
            if len(test.columns) == 22:
                self.dataformat = 0
            else:
                self.dataformat = 1
            if self.dataformat == 0:
            #result = pd.read_csv(result_fn,usecols=[1,5],index_col=0)
                result = pd.read_csv(result_fn,usecols=[1,8],index_col=0)
                result['Channel'] = self.channel
            elif self.dataformat == 1:
                result = pd.read_csv(result_fn,usecols=[0,4,8,11,13,14],index_col=1)
                result['Channel'] = result['FileName'].str.split('_').str[1]
                result['Channel'] = result['Channel'].replace({"0":"Blue","1":"Green","2":"Red","3":"NIR"})
            #We use temperature corrected DEPrime
            #result = result.rename(columns={' DEPrime_NoTemp ':"DEPrime"})
            result.columns = result.columns.str.strip()
        except:
            print("no RDP results available")
            self.results['Channel'] = self.channel #make sure that 'channel' is defined
            return
        self.results = self.results.join(result)
        #sort by labels. We need to add cahnnel to the index. otherwise we will have duplicated indices which is not allowed for reindex
        self.results = self.results.set_index(["labels","Channel"])
        self.results = self.results.reindex(index=natsort.natsorted(self.results.index))
        self.results = self.results.reset_index()
        
        
    def _generate_results_tables(self):
        '''
        generates tables with the same layout as the DV plates for DEPrime and Temperature
        '''
        results_table = self.results.copy()
        results_table["Columns"] = results_table.labels.apply(lambda x: x[0])
        results_table["Rows"] = results_table.labels.apply(lambda x: x[1:])
        if "DEPrime" in self.results:
            
            try: #if no results available this fails...
                results_table_DEPrime = results_table.set_index(["Columns","Rows","Channel"])["DEPrime"].unstack(level=0)
                results_table_DEPrime.index = results_table_DEPrime.index.set_levels(results_table_DEPrime.index.levels[0].astype("int"), level=0)
                results_table_DEPrime.sort_values(axis=0,by="Rows",inplace=True)
                self.results_table = results_table_DEPrime
            except:
                self.results_table = None
        else:
            self.results_table = None
        
        if "temperature" in self.results:
            try: #if no results available this fails...
                temp_table = results_table.set_index(["Columns","Rows","Channel"])["temperature"].unstack(level=0)
                temp_table.index = temp_table.index.set_levels(temp_table.index.levels[0].astype("int"), level=0)
                temp_table.sort_values(axis=0,by="Rows",inplace=True)
                self.temp_table = temp_table
            except:
                self.temp_table = None
        else:
            #temperature not available
            self.temp_table = None
        
    
    def _load_params(self):
        '''
        Loads the measurement parameters in the subfolder Readerfiles
        '''
        self.params = pd.DataFrame({"temperature":[]})
        prefix_dict = {"Blue": "0","Green":"1","Red":"2","NIR":"3"}
        for vial in self.results.index.unique():
            #For multi color reads this works. 
            #but for single color reads is is sometimes 0. therefore just take the first file found. 
            #path = os.path.join(self.path,self.measurement,"ReaderFiles",f"ch_{prefix_dict[self.channel]}_APD_Read_Log_{vial}_V1.xml")
            file_list = glob(os.path.join(self.path,self.measurement,"ReaderFiles",f"ch_*_APD_Read_Log_{vial}_V1.xml"))
            #path = os.path.join(self.path,self.measurement,"ReaderFiles",f"ch_0_APD_Read_Log_{vial}_V1.xml")
            if len(file_list)==0:
                continue
            path = file_list[0]
            try:
                with open(path,"r") as markup:
                    bs = BeautifulSoup(markup,"lxml")
            except FileNotFoundError:
                #print(f"not able to read measurement parameters for vial {vial}")
                continue
            parameters_string = bs.find("MeasurementParameters".lower()).decode().replace("\"","")
            param_dict = {entry.split("=")[0]:entry.split("=")[1] for entry in parameters_string.split()[1:-1]}
            
            #add measurement time to params
            #we cannot read time from BeautifulSoup
            try:
                with open(path,"r") as markup:
                    text = markup.read()
                regex = re.compile(r'MeasurementDate="(.*)\+')
                date = regex.findall(text)[0]
                #print(f"date: {date}")
                date = datetime.strptime(date[:26], '%Y-%m-%dT%H:%M:%S.%f')
                param_dict['time'] = date
            except:
                #print("date not found")
                pass
            
            params = pd.DataFrame(param_dict,index=[vial])
            self.params = self.params.append(params)
                
            
        
        #cast temperature to float
        self.params['temperature'] = self.params['temperature'].astype(float)
        #copy temperatures and times to results
        self.results["temperature"] = self.params['temperature']
        try:
            self.results["time"] = self.params['time']
        except KeyError:
            pass
            

    
    def _set_conc(self):
        if self.plate_map is not None:
            for label in self.results['labels']:
                dye = None
                if self.plate_map == "OQ":
                    if int(label[1:]) in [1,2,3,6,7,10,11,12]:
                        conc=30
                    if int(label[1:]) in [8,9]:
                        conc=60
                    if int(label[1:]) in [5]:
                        conc=12
                    if int(label[1:]) in [4]:
                        conc=0
                elif self.plate_map == "Titration":
                    row1_dict = {"A":20000, "B":6667, "C":2222, "D":741,
                                "E": 247, "F": 82, "G":27, "H":9}
                    row2_dict = {"A":0, "B":0, "C":6, "D":12,
                                "E": 21, "F": 30, "G":45, "H":60}
                    if int(label[1:]) in [1,3,5]:
                        conc = row1_dict[label[0]]
                    if int(label[1:]) in [2,4,6]:
                        conc = row2_dict[label[0]]
                elif self.plate_map == "2Titration":
                    column_dict = {"1":0, "2":0, "3":1000, "4":500,
                                "5": 250, "6":125, "7":62.5,"8":31.2,"9":15.6,"10":0,"11":0,"12":0}
                    conc = column_dict[label[1:]]
                elif self.plate_map == "OQTitration":
                    column_dict = {"1":0, "2":15, "3":30, "4":60,
                                "5": 0, "6":15, "7":30,"8":60,"9":0,"10":15,"11":30,"12":60}
                    conc = column_dict[label[1:]]
                elif self.plate_map == "ANOVA":
                    column_dict = {"1":0, "2":12, "3":30, "4":60,
                                "5": 0, "6":12, "7":30,"8":60,"9":0,"10":0,"11":0,"12":0}
                    conc = column_dict[label[1:]]
                elif self.plate_map == "ANOVA2":
                    column_dict = {"1":0, "2":12, "3":30, "4":60,
                                "5": 120, "6":0, "7":12,"8":30,"9":60,"10":120,"11":0,"12":0}
                    conc = column_dict[label[1:]]
                elif self.plate_map == "GREEN":
                    row_dict = {"A":6666,"B":2222, "C":2222/3, "D":246,
                                "E": 82, "F":27, "G":9.1,"H":0}
                    column_dict = {"1":"Seta555", "2":"Seta555", "3":"Atto532","4":"Atto532","5":"Alexa532", "6":"Alexa532",
                                   "7":"Promo532", "8":"Promo532", "9":"Daylight550","10":"Daylight550","11":"","12":""}
                    conc = row_dict[label[0]]

                    dye = column_dict[label[1:]]
                elif self.plate_map == "GREEN2":
                    column_dict = {"1":0, "2":111/8, "3":111/4, "4":111/2,
                                "5": 111, "6":222, "7":666,"8":0,"9":0,"10":0,"11":0,"12":0}
                    conc = column_dict[label[1:]]
                elif self.plate_map == "GREEN3":
                    column_dict = {"1":0, "2":12.5, "3":25, "4":50,
                                "5": 100, "6":200, "7":6666,"8":0,"9":0,"10":0,"11":0,"12":0}
                    conc = column_dict[label[1:]]
                elif self.plate_map == "GREEN4":
                    column_dict = {"1":0, "2":int(1000/(3**5)), "3":int(1000/(3**4)), "4":int(1000/(3**3)),
                                "5": int(1000/(3**2)), "6":int(1000/3), "7":1000,"8":0,"9":0,"10":0,"11":0,"12":0}
                    conc = column_dict[label[1:]]
                    dye = "Alexa532"
                elif self.plate_map == "GREEN5":
                    column_dict = {"1":0, "2":1000, "3":int(1000/(3**1)), "4":int(1000/(3**2)),
                                "5": int(1000/(3**3)), "6":int(1000/(3**4)), "7":int(1000/(3**5)),"8":0,"9":0,"10":0,"11":0,"12":0}
                    conc = column_dict[label[1:]]
                    dye = "Alexa532"
                elif isinstance(self.plate_map, (float, int)):
                    conc = self.plate_map
                elif isinstance(self.plate_map, list):
                    conc = self.plate_map[int(label[1:])-1]
                else:
                    raise Exception("plate map not found")
                    conc = None
                
                self.results.loc[label,f"Conc_{self.channel}"] = conc
                if dye is not None:
                    self.results.loc[label,"dye"] = dye
        else:
            try:
                for channel in ['Blue', 'Green', 'Red' , 'NIR']:
                    rec_conc = pd.read_excel(self.path/self.measurement/"ExperimentParameters.xlsx",sheet_name = channel,header = 2,skiprows=0,nrows = 8)
                    rec_conc = rec_conc.rename(columns ={"Unnamed: 0":"Column"})
                    rec_conc = rec_conc.melt(id_vars=["Column"],var_name = "Row",value_name=f"Conc_{channel}")
                    rec_conc["labels"] = rec_conc["Column"]+rec_conc["Row"].astype(str)
                    rec_conc = rec_conc.set_index("labels")
                    rec_conc = rec_conc.drop(columns = ["Row","Column"])
                    rec_conc = rec_conc.fillna(0)
                    self.results = pd.merge(self.results.set_index("labels"),rec_conc,left_index=True, right_index=True).reset_index()
            except:
                print("ExperimentParameters.xlsx not available. Concentrations not loaded")
                
    
    def calc_nsigma(self,data, threshold_quantile):
        '''
        Calculates nsigma for the SMC algorith from the threshold_quantile and the data based on a robust estimate of lambda
        :param data: data set of counts
        :type data: np.ndarray
        :param threshold_quantile: quantile
        :type threshold_quantile: float
        '''
        if  self.rpy2_loaded:
            df = pd.DataFrame({"y": data})
            with localconverter(robjects.default_converter + pandas2ri.converter):
                r_data = robjects.conversion.py2ri(df)
            fit = r_robustbase.glmrob("y~1",data=r_data,family="poisson",method= "Mqle")
            _lambda = np.exp(r_stats.coef(fit)[0])
        else:
            #logging info removed because it pops up in every simulation step
            #self.logger.info("crpy2 not loaded. Cannot calculate threshold based on robust statistics")
            _lambda = np.median(data)

        background_dist = scipy.stats.poisson(_lambda)
        threshold_counts = background_dist.ppf(threshold_quantile)
        nsigma =  (threshold_counts-_lambda)/np.sqrt(_lambda)
        return nsigma
    
    
    
    def calc_DEPrime(self,vials_to_count = ["..+"],threshold=None, normalisation = 60, channel = None ):
        '''
        Calculates DEPrime based on Singulex' algorithm and stores the result in self.results.DEPrime_NoTemp
        
         Parameters:
        -----------
        :param vials_to_count: Vials for which DE' shall be calculated
        :param threshold: threshold. If <1 it is enterpreted at quantile, else as threshold above mean value in units of standard deviation of the data
        :param normalisation: Number of seconds of read time to which DE' shall be normalised
        :param channel: Channel to use
        '''
        
        if threshold is None:
            threshold = self.calc_params["threshold"]
        if normalisation is None:
            normalisation = self.calc_params["normalisation"]
        if vials_to_count is None: 
            vials_to_count = self.calc_params["Vials_to_count"]
        if channel is None:
            channel = self.channel
            
        
        
        plot_list = []
        for pattern in vials_to_count:
            l = [vial for vial in self.results['labels'] if re.search(pattern,vial)]
            plot_list +=l
        self.plot_list = plot_list
        if len(plot_list) == 0:
            print("No vials selected")
            return
        
        for i, vial in enumerate(plot_list):
            read = self.data[vial][channel]
            if threshold < 1:
                ''' update nsigma based in a robust estimate'''
                nsigma = self.calc_nsigma(read,threshold)
            else:
                nsigma = threshold
            self.calc_params["nsigma"] = nsigma
            self.adjust_SMC_values('nsigma')
            
            nr_events, threshold_counts = self._calculate_events_smc(read)
            #print(nr_events)
            #if self.calc_params["normalisation"]>0:
            #    nr_events *= normalisation / (self.bintime*read.shape[0])
            self.results = self.results.set_index(["labels","Channel"])
            self.results.loc[(vial,channel),"DEPrime_NoTemp"] = nr_events
            self.results.loc[(vial,channel),"Threshold"] = threshold_counts
            self.results = self.results.reset_index()
                
                    
    
    def _calculate_events_smc(self,data):
        '''
        calculates the number of events with the SMC algorithm
        Parameter:
        -------
        :param data: data set of counts. 
        '''
        result_dummy = int(1)
        threshold_dummy = int(1)
        if self.SMC_calculator is not None:
            nr_events, threshold_counts = self.SMC_calculator.addPhotonData(data.values, len(data),result_dummy,threshold_dummy)[1:3]
        else:
            self.logger.error("SMC_calculator not loaded. DE' cannotbe calculated")
            nr_events, threshold_counts = 0,0
        #revert normalisation to 60s 
        #nr_events /=(60/self.scan_dict['scan_time'])
        #nr_events /=(600000/len(data))
        #nr_events /= (60/self.scan_dict["scan_time"])
        return nr_events, threshold_counts
            
        
    def adjust_SMC_values(self, which=('all',)):
        '''
        Adjusts the file DefaultValues.xml used by the SMC algorithm
        '''
        if isinstance(which, str):
            which = (which,)
        
        if os.path.exists(self.SMC_actual_values_path):
            with open(self.SMC_actual_values_path,"r") as f:
                data = f.read()
            
            if len(set(which) & set(['all', 'bin_length'])):
                regex = re.compile(r"(?<=<BinReadTime>)\d+(?=<\/BinReadTime>)",re.MULTILINE)
                bin_length = self.bintime*10**6
                try:
                    data = re.sub(regex, f"{bin_length:.0f}", data, 1)
                except Exception:
                    self.logger.error("regex not found", exc_info=True)
            
            if len(set(which) & set(['all', 'nsigma'])):
                regex = re.compile(r"(?<=<NSigma>).*(?=<\/NSigma>)",re.MULTILINE)
                nsigma = self.calc_params["nsigma"]
                try:
                    data = re.sub(regex, f"{nsigma:.2f}", data, 1)
                except Exception:
                    self.logger.error("regex not found", exc_info=True)

            with open(self.SMC_actual_values_path,"w") as f:
                f.write(data)
    
    def calc_events(self,vials_to_count = ["..+"], corrected=None,threshold=None,windows = None, stats = None, normalisation = 60, channel = None):
        '''
        Parameters:
        -----------
        vials_to_count: List of the vials to plot. A Regex string can be used. e.g. ["..+"] plots all vials
        corrected:  If True: use baseline correction
        threshold:  quantil to use as threshold
        windows:    if > 1: only count events whicha are at least as windows points broad. 
                    else: count broader events as one event
        stats:      Statistics used to calculate number of events:
                    "Pois": use individual measurement to estimate background distribution by using median as an robust estimate of the distribution parameter
                    "Pois_0ppm": Use 0ppm measurements to estimate poisson distributed background
                    "Norm_0ppm": use 0ppm measurements to estimate background distribution
                    "Norm"; use individual measurement to estimate background distribution by using median as an robust estimate of the distribution parameter
        normalisation: Normalises the number of counts to the given measurement time (RDP normalises to 60s)
        channel: channel to use
        Return
        ------
        None
        '''
        if corrected is None:
            corrected = self.calc_params["corrected"]
        if threshold is None:
            threshold = self.calc_params["threshold"]
        if windows is None:
            windows = self.calc_params["windows"]
        if stats is None:
            stats = self.calc_params["stats"]
        if normalisation is None:
            normalisation = self.calc_params["normalisation"]
        if vials_to_count is None: 
            vials_to_count = self.calc_params["Vials_to_count"]
        if channel is None:
            channel = self.channel
        new_param_dict = dict(corrected=corrected,threshold=threshold,windows = windows, stats = stats, normalisation= normalisation, vials_to_count = vials_to_count, channel = channel)
        
        if not self.calc_params==new_param_dict:
            self.calc_params=new_param_dict
            self._isCalculated = False
                
        if not self._isCalculated:
            plot_list = []
            for pattern in vials_to_count:
                l = [vial for vial in self.results['labels'] if re.search(pattern,vial)]
                plot_list +=l
            self.plot_list = plot_list
            if len(plot_list) == 0:
                print("No vials selected")
                return
            
            for i, vial in enumerate(plot_list):
                read = self.data[vial][channel]
                read["Count"] = read["Count_raw"].copy()
                if corrected:
                    read["Count"] = read["Count"] - np.convolve(read["Count"],np.repeat(1,40)/40,mode="same")
                    
                    
            if "Pois" in stats:
                events = self._calc_events_poisson(plot_list,threshold, stats)
            elif "Norm" in stats:
                events = self._calc_events_norm(plot_list,threshold, stats)

                '''
                #use rolling MAD to make a robust estimation of the variance in the photon noise
                rolling_mad = read["Count"].rolling(40, min_periods=1).apply(self.mad, raw=True)
                #rolling_quantile = read["Count"].rolling(40, min_periods=1).quantile(0.1)
                rolling_mean = read["Count"].rolling(40, min_periods=1).mean()
                #events = 1*(read["Count"]>read["Count"].std()*threshold+read["Count"].mean())
                #events = 1*(read["Count"]>read["Count"].std()*threshold+rolling_mean)
                #_threshold = rolling_mad*1.5*threshold+rolling_mean
                _threshold = rolling_mad*1.5*threshold+rolling_mean
                events = 1*(read["Count"]>_threshold)
                '''
            else:
                raise Exception(f"Statistics {stats} not implemented")
                 
            for i, vial in enumerate(plot_list):
                if windows > 1:
                    self.data[vial][channel]["event"] = np.convolve(self.data[vial][channel]["event"],np.repeat(1,windows)/windows, mode="same")
                else:
                    self.data[vial][channel]["event"] = np.diff(self.data[vial][channel]["event"],append=[0])
                nr_events = np.sum(self.data[vial][channel]["event"]==1)
                norm_factor = 1
                if normalisation is not None:
                    norm_factor = normalisation  /(self.bintime*self.data[vial][channel]["event"].shape[0])
                self.results.loc[vial,"events"] = norm_factor*nr_events
            try:
                self.calculate_regression(vials_to_count = vials_to_count,x = channel)
            except:
                print("regression not calculated")
            self._isCalculated = True
    
    def _calc_events_poisson(self, plot_list,threshold,method,channel = None):
        '''
        calcultes the events based on the assumption of a poisson distributed background signal.
        Parameters:
        plot_list: vials to calculate
        method:  method to use:
                "Pois_0ppm"; use 0ppm measurements to estimate background distribution
                "Pois": use individual measurement to estimate background distribution by using median as an robust estimate of the distribution parameter
        channel: Channel to use
        '''
        if channel is None:
            channel = self.channel
        if method.split("Pois")[1] == "_0ppm":
            mu_data = np.zeros(self.nr_bins)
            zero_conc_labels = self.results.loc[(self.results[f"Conc_{self.channel}"]==0) & (self.results['labels'].isin(plot_list))]['labels']
            if zero_conc_labels.shape[0] == 0:
                raise Exception("No Zero concentration vials available for Poisson background estimation")
            else:
                for label in zero_conc_labels:
                    mu_data += self.data[label][channel]['Count'][:self.nr_bins]
                mu_data /= zero_conc_labels.shape[0]
                self.background_median = np.median(mu_data)
                self.background_dist = scipy.stats.poisson(self.background_median)
                self.event_threshold = self.background_dist.ppf(threshold)
            
            for label in plot_list:
                self.results.loc[label,"threshold"] = self.event_threshold
        
        elif method.split("Pois")[1] == "":            
            for label in plot_list:
                median = np.median(self.data[label][channel]['Count'])
                background_dist = scipy.stats.poisson(median)
                self.results.loc[label,"threshold"] = background_dist.ppf(threshold)
        
            
        else:
            raise Exception(f"Method {method} not implemented")
        
        for label in plot_list:
            self.data[label][channel]["event"] = 1*(self.data[label][channel]["Count"]>self.results.loc[label,"threshold"])
        
    def _calc_events_norm(self, plot_list,threshold,method):
        '''
        calcultes the events based on the assumption of a Normal distributed background signal.
        Parameters:
        plot_list: vials to calculate
        method:  method to use:
                "Norm_0ppm": use 0ppm measurements to estimate background distribution
                "Norm"; use individual measurement to estimate background distribution by using mean as an robust estimate of the distribution parameter
        '''
        if method.split("Norm")[1] == "_0ppm":
            mu_data = np.zeros(self.nr_bins)
            zero_conc_labels = self.results.loc[(self.results[f"Conc_{self.channel}"]==0) & (self.results['labels'].isin(plot_list))]['labels']
            if zero_conc_labels.shape[0] == 0:
                raise Exception("No Zero concentration vials available for Poisson background estimation")
            else:
                for label in zero_conc_labels:
                    mu_data += self.data[label]['Count'][:self.nr_bins]
                mu_data /= zero_conc_labels.shape[0]
                self.background_median = np.median(mu_data)
                self.background_std = np.std(mu_data)*np.sqrt(zero_conc_labels.shape[0])
                self.background_dist = scipy.stats.norm(loc=self.background_median,scale = self.background_std)
                self.event_threshold = self.background_dist.ppf(threshold)
            
            for label in plot_list:
                self.results.loc[label,"threshold"] = self.event_threshold
        
        elif method.split("Norm")[1] == "":            
            for label in plot_list:
                background_median = np.median(self.data[label]['Count'])
                background_std = np.std(self.data[label]['Count'])
                background_dist = scipy.stats.norm(loc=background_median,scale = background_std)
                self.results.loc[label,"threshold"] = background_dist.ppf(threshold)
        
            
        else:
            raise Exception(f"Method {method} not implemented")
        
        for label in plot_list:
            self.data[label][channel]["event"] = 1*(self.data[label]["Count"]>self.results.loc[label,"threshold"])
            
        
        
        
    
    
    def calculate_regression(self,vials_to_use = ["..+"],x = None,y ="DEPrime",channel = None, data = None):
        '''
        calculates a linear regression of the events vs. concentration
        '''
        if x is None:
            x = f"Conc_{self.channel}"
        if x in ['Blue', 'Green', 'Red' , 'NIR']:
           x = f"Conc_{x}" 
        if data is None:
            data = self.results
        if channel is None:
            channel = self.channel
        vial_list = []
        
        for pattern in vials_to_use:
            l = [vial for vial in pd.unique(data['labels']) if re.search(pattern,vial)]
            vial_list +=l
        data_to_use = data[(data['labels'].isin(vial_list)) & (data['Channel'] == channel)]
        if not((x in data_to_use) and (y in data_to_use)):
            print("Regression not possible. Wrong varibales")
            self.LOD = np.nan
            self.std_background = np.nan
            self.offset = np.nan
            return
        self.ols = sm.ols(formula =  f"{y} ~ {x}",data = data_to_use).fit()
        #self.LOD = 2*self.results.loc[self.results[f'Conc_{channel}']==0,y].std()/self.ols.params[f'Conc_{channel}']
        if 'Conc_' in x:
            self.std_background = data_to_use.loc[data_to_use[x]==0,y].std()
            if self.std_background > 0: #make sure that background sdt is not zero. Otherwise LOD is nan. 
                self.LOD = 2*self.std_background/self.ols.params[1]
            else:
                self.LOD = np.nan
            self.offset = data_to_use.loc[data_to_use[x]==0,y].mean()/self.ols.params[1]

    def plot_events(self,vials_to_plot = ["..+"], ax = None,channel = None, **args):
        '''
        Plots the counts vs concentration
        
        Parameters:
        --------
        vials_to_plot : list of regex strings which can be used select vials to plot
                        Examples:
                        ["..+"] : Plot All vials
                        ["A1$","A2$"]: Plot vials A1 and A2
                        ["A[1-2]$"] Plot vials A1 and A2
                        
        ax: matplotlib axis to use
        **args: arguments used to recalculate results. (see calc_events())
        '''
        if channel is None:
            channel = self.channel
        args.update({"vials_to_count":vials_to_plot})
        self.calc_events(**args)
        if 'events' not in self.results:
            print("No data to plot")
            return
        if ax is None:
            fig,ax = plt.subplots()
        else:
            ax = ax
        vial_list = []
        for pattern in vials_to_plot:
            l = [vial for vial in self.results['labels'] if re.search(pattern,vial)]
            vial_list +=l 
        results_to_plot = self.results[self.results['labels'].isin(vial_list)]
        results_to_plot = results_to_plot[results_to_plot['Channel'] == channel]
        results_to_plot.plot.scatter(x = f"Conc_{self.channel}",y="events",ax=ax)
        if self.ols is not None:
            x_range = np.arange(results_to_plot[f"Conc_{self.channel}"].min(),results_to_plot[f"Conc_{self.channel}"].max())
            ax.plot(x_range,self.ols.predict(pd.DataFrame({f"Conc_{self.channel}":x_range})),color="red")
            max_pos = ax.get_ylim()[0]+np.linspace(0.6,0.9,4)*np.array(ax.get_ylim()).ptp()
            ax.text(0,max_pos[0],"R: "+str(round(self.ols.rsquared,3)))
            ax.text(0,max_pos[1],"Slope: "+str(round(self.ols.params[1],3)))
            ax.text(0,max_pos[2],"LOD: "+str(round(self.LOD,3)))
            ax.text(0,max_pos[3],"offset: "+str(round(self.offset,3)))
        ax.figure.tight_layout()
        self.cursor = mplcursors.cursor(ax,hover=True)
        cursor_text = lambda sel: sel.annotation.set_text(f"{results_to_plot.iloc[sel.target.index]['labels']}, {results_to_plot.iloc[sel.target.index]['Conc_'+self.channel]}fM: {(results_to_plot.iloc[sel.target.index]['events']):.0f}")
        self.cursor.connect( "add", cursor_text)
        return ax
        
    
    def plot_DE(self,vials_to_plot = ["..+"],text = True,regression = True,  ax = None,channel = None,y = 'DEPrime',x = None,**kwargs):
        '''
        Plots the results from the RDP software
        Parameters:
        ----------
        vials_to_plot : list of regex strings which can be used select vials to plot
                        Examples:
                        ["..+"] : Plot All vials
                        ["A1$","A2$"]: Plot vials A1 and A2
                        ["A[1-2]$"] Plot vials A1 and A2
        text:           if True, values of Slope, LOD and R^2 are printed
        regression:     if True, a regression line is plotted
        ax: matplotlib axis to use
        **kwargs:       Additional kwargs for plot function
        '''
        if channel is None:
            channel = self.channel
        if x is None:
            x = channel
        if x in ['Blue', 'Green', 'Red' , 'NIR']:
           x = f"Conc_{x}" 
        if 'DEPrime' not in self.results:
            print("RDP result file not available")
            return
        if ax is None:
            fig,ax = plt.subplots()
        vial_list = []
        for pattern in vials_to_plot:
            l = [vial for vial in pd.unique(self.results['labels']) if re.search(pattern,vial)]
            vial_list +=l 
        results_to_plot = self.results[self.results['labels'].isin(vial_list)]
        results_to_plot = results_to_plot[results_to_plot['Channel'] == channel]
        if isinstance(y,str):
            y = [y]
        for quantity in y:
            results_to_plot.plot.scatter(x = x,y=quantity,ax=ax,**kwargs)
        ax.set_xlabel("concentration / fM")
        #ols = sm.ols(formula =  f"DEPrime ~ Conc_{self.channel}",data = results_to_plot).fit()
        for quantity in y:
            ols = self.calculate_regression(data = results_to_plot, x = x, y = quantity, channel = channel)
        #from statsmodels.genmod.families import Poisson
        #ols = sm.glm("DEPrime ~ conc",data = result,family=Poisson()).fit()
            if ("DEPrime" in quantity) and ("Conc_" in x):
                LOD = 2*results_to_plot.loc[results_to_plot[x]==0,quantity].std()/self.ols.params[x]
                offset = results_to_plot.loc[results_to_plot[x]==0,quantity].mean()/self.ols.params[x]
            x_range = np.arange(results_to_plot[x].min(),results_to_plot[x].max())
            if 'color' in kwargs:
                color = kwargs['color']
            else:
                color = 'red'
            if regression:
                ax.plot(x_range,self.ols.predict(pd.DataFrame({x:x_range})),color=color)
            if text:
                max_pos = ax.get_ylim()[0]+np.linspace(0.6,0.9,4)*np.array(ax.get_ylim()).ptp()
                ax.text(0,max_pos[0],"R: "+str(round(self.ols.rsquared,3)))
                ax.text(0,max_pos[1],"Slope: "+str(round(self.ols.params[1],2)))
                if ("DEPrime" in quantity) and ("Conc_" in x):
                    ax.text(0,max_pos[2],f"LOD: {LOD:.2f} fM")
                    ax.text(0,max_pos[3],f"Offset: {offset:.2f} fM")
            if not ax.figure.get_constrained_layout():        
                ax.figure.tight_layout()
            self.cursor = mplcursors.cursor(ax,hover=True)
            cursor_text = lambda sel: sel.annotation.set_text(f"{results_to_plot.iloc[int(sel.target.index)]['labels']}, {results_to_plot.iloc[int(sel.target.index)]['Conc_'+channel]}fM: {(results_to_plot.iloc[int(sel.target.index)][quantity]):.0f}")
            self.cursor.connect( "add", cursor_text)
        return ax

    def plot_vials(self, vials_to_plot,corrected=False, data = "Read", channel = None, ax = None,legend = True, DE = False,threshold=None, **kwargs):
        '''
        Plots the raw data of the given vials
        Parameters:
        ----------
        vials_to_plot : list of regex strings which can be used select vials to plot
                        Examples:
                        ["..+"] : Plot All vials
                        ["A1$","A2$"]: Plot vials A1 and A2
                        ["A[1-2]$"] Plot vials A1 and A2
        corrected: if true the baseline corrected data is plotted
        data: either "Read" or "Focus"
        channel: channel to plot
        ax: matplotlib ax to plot. if an array is given, the individual vials are plotted in separat axes
        legend: if True plot legend
        DE: If True the calculate DE Value is printed.
        Threshold: if not None, the threshold is plotted als horzontal red line. The value of the parameter is used as mulitplier (default=5.5)
        '''
        if channel is None:
            channel = self.channel
        plot_list = []
        if isinstance(vials_to_plot,str):
            #make list out of vials_to_plot if it is a string (single pattern)
            vials_to_plot = [vials_to_plot]
        for pattern in vials_to_plot:
            l = [vial for vial in pd.unique(self.results['labels']) if re.search(pattern,vial)]
            plot_list +=l 
        if ax is None:
            fig, ax = plt.subplots()
        if data == "Read":
            plot_data = self.data
            x_data = "bin"
        elif data == "Focus":
            plot_data = self.focus_data
            x_data = "Position"
        else:
            print("Wrong data selected. Choose 'Read' or 'Focus'")
            return
        y_data = "Count" if corrected else "Count_raw"
            
        for i, vial in enumerate(plot_list):
            vial_data = plot_data[vial][channel] if  data == "Read" else plot_data[vial]
            label = f"{vial}:{channel}" if "label"  not in kwargs else kwargs["label"]
            if type(ax) is list or type(ax) is np.ndarray:
                vial_data.plot(y=y_data,ax = ax[i], label=label)
                ax[i].set_xlabel(x_data)
                ax[i].legend(loc=1)
                ax[i].legend_.set_visible(legend)
                if threshold:
                    threshold_value = self.results[self.results["Channel"]==channel].loc[vial,"AvgEstNoise"]
                    threshold_value = threshold_value + threshold*np.sqrt(threshold_value)
                    ax[i].axhline(y = threshold_value, color="Red",linestyle="--")
                if DE:
                    x_pos = np.array(ax[i].get_xlim()).mean()
                    y_pos = np.quantile(np.array(ax[i].get_ylim()),0.9)
                    try:
                        DE_value = self.results[self.results["Channel"]==channel].loc[vial,"DEPrime"]
                    except KeyError:
                        DE_value= np.nan
                    ax[i].text(x_pos,y_pos,f"DE: {DE_value:.0f}", ha='center', va='center', fontsize="x-small")
                

            else:
                vial_data.plot(y=y_data,ax = ax, label=label)
                ax.set_xlabel(x_data)
                ax.legend(loc=1)
                ax.legend_.set_visible(legend)
                if threshold:
                    threshold_value = self.results[self.results["Channel"]==channel].loc[vial,"AvgEstNoise"]
                    threshold_value = threshold_value + threshold*np.sqrt(threshold_value)
                    ax.axhline(y = threshold_value, color="Red",linestyle="--")
                if DE:
                    x_pos = np.array(ax.get_xlim()).mean()
                    y_pos = np.quantile(np.array(ax.get_ylim()),0.9)
                    DE_value = self.results[self.results["Channel"]==channel].loc[vial,"DEPrime"]
                    ax.text(x_pos,y_pos,f"DE: {DE_value:.0f}", ha='center', va='center',fontsize="x-small")
                
            
            
        return ax
    
    def plot_all_vials(self,data="Read",channel=None,legend = False, DE = False,threshold=None,ylim=None,sharex=True, sharey=True,*kwargs):
        rows = pd.unique(self.results.row)
        columns = pd.unique(self.results.column)
        fig, axes = plt.subplots(rows.size,columns.size, sharex=sharex, sharey=sharey,figsize=[10,10])
        for axs, row in zip(axes,list(rows)):
            self.plot_vials([f"{x}{row}$" for x in list(columns)],ax = axs, data = data, channel = channel,legend=legend,DE=False,threshold=threshold, *kwargs)
            #remove legend
            #for ax in axs:
            #    ax.legend_.set_visible(False)
        
        
        pad = 5
        for ax, row in zip(axes[:,0], rows):
            ax.set_ylabel("counts / bin")
            ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                        xycoords=ax.yaxis.label, textcoords='offset points',
                        size='large', ha='right', va='center',rotation=90)
            for ax, col in zip(axes[0,:], columns):
                ax.set_title(col)
        #set ylim to make sure that DE values are printed at the correct position. 
        if ylim:
            axes[0,0].set_ylim(ylim[0],ylim[1])
        axes[0,0].figure.canvas.draw()
        if DE:  
            for axs, row in zip(axes,list(columns)):
                for ax, column in zip(axs,list(rows)):
                    x_pos = np.array(ax.get_xlim()).mean()
                    y_pos = np.quantile(np.array(ax.get_ylim()),0.9)
                    try:
                        DE_value = self.results[self.results["Channel"]==channel].loc[f"{row}{column}","DEPrime"]
                    except KeyError:
                        DE_value = np.nan
                    ax.text(x_pos,y_pos,f"DE: {DE_value:.0f}", ha='center', va='center',fontsize="x-small")
        return fig, axes
        
        
    def plot_CV(self,value ="DEPrime",color = "relative", annot = "absolute",ax = None,channel = None,  **kwargs):
        '''
        Plots a heat map of the DE' counts. 

        The Rows and Columns are annotated by the coefficients of variance in the 
        respective column/row. 
        
        parameters:
        -----------
        value: value to plot "DEPrime" or "temperature"
        color: select data for color coding:
            "absolute": absolute DE'
            "relative": relative deviations in % from row mean
            
        annot: select data for annotation:
            "absolute": absolute DE'
            "relative": relative deviations in % from row mean
        ax: matplotlib axis used for plotting. If None a new figure is generated
        channel: channel to plot
        **kwargs: additional keyword arguments for seaborn.heatmap
        
        return:
        --------
        matplotlib axis
        
        '''
        #copy the results and reshape
        #res = self.results.copy()
        #res["Columns"] = res.labels.apply(lambda x: x[0])
        #res["Rows"] = res.labels.apply(lambda x: x[1:])
        
        #res_rearanged = res.groupby(["Columns","Rows"])["DEPrime"].first().unstack(level=0)
        #res_rearanged.index = res_rearanged.index.astype("int")
        #res_rearanged.sort_values(axis=0,by="Rows",inplace=True)
        #self.results_table = res_rearanged
        #Calculate row-wise relative changes
        if channel is None:
            channel = self.channel
        if value == "DEPrime":
            results_table_relative = self.results_table.xs(channel,level="Channel").copy()
        elif value == "temperature":
             results_table_relative = self.temp_table.copy()
        results_table_relative = results_table_relative.transpose()
        results_table_relative = results_table_relative.apply(lambda x: (x-np.nanmean(x))/np.nanmean(x)).transpose()
        results_table_relative *=100
        #Calculate row- and column-wise CV
        data = self.results_table.xs(channel,level="Channel").values
        cv_column = np.nanstd(data, axis=0) / np.nanmean(data,axis=0)
        cv_row = np.nanstd(data, axis=1) / np.nanmean(data, axis=1)
        #x and y labels are the coefficient of variances along columns and rows respectively
        column_labels = [f"{label*100:.2f}" for label in  cv_column]
        row_labels = [f"{label*100:.2f}" for label in  cv_row]
        
        if ax == None:
            fig, ax = plt.subplots()
        if color == "relative":
            plot_data = results_table_relative
            cbar_label = "Deviation / %"
        elif color == "absolute":
            
            plot_data = self.results_table.xs(channel,level="Channel") if value == "DEPrime" else self.temp_table
            
            cbar_label = value
        else:
            print("color must be 'absolute' or 'relative'")
            return
        if annot == "relative":
            annot_data = results_table_relative
        elif annot == "absolute":
            annot_data = self.results_table.xs(channel,level="Channel") if value == "DEPrime" else self.temp_table
        else:
            print("annot must be 'absolute' or 'relative'")
            return
        heatmap_center =np.nanmean(plot_data.values)
        
        format_string = '.0f' if value=="DEPrime" else '.1f'
        heat_map = sb.heatmap(plot_data,annot = annot_data,fmt=format_string, center = heatmap_center, xticklabels=column_labels, yticklabels=row_labels,ax = ax,
            cbar_kws={"shrink":.9, "label": cbar_label},**kwargs)
        heat_map.set_xticklabels(heat_map.get_xticklabels(), rotation=90)
        heat_map.set_xlabel("Columns CV / %")
        heat_map.set_ylabel("Rows CV / %")
        heat_map.set_title("A1 is top left, H12 bottom right")
        heat_map.figure.tight_layout()
        return heat_map
        
    def stats_table(self, channel = None):
        ''' calculates a table with statistics of DE' grouped by concentration
        Parameters:
        channel: channel to use, if None, self.channel is used
        '''
        if channel is None:
            channel = self.channel
        grouped_data = self.results[self.results["Channel"]==channel].groupby(f"Conc_{channel}")['DEPrime']
        means = grouped_data.mean().to_frame()
        means.columns = ["Mean"]
        std = grouped_data.std().to_frame()
        std.columns = ["STDEV"]
        stats = means.join(std)
        stats['CV'] = stats['STDEV']/stats['Mean']*100
        return stats
        
